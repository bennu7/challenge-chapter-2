// nama : Lalu Ibnu Hidayatullah
const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function getData(q) {
    return new Promise((resolve, rejects) => {
        rl.question(q, a => {
            resolve(a);
        })
    })
}


async function main() {
    let input;
    let arrayValue = [];

    while (input !== "q") {
        input = await getData("nilai : ");

        data = arrayValue.push(input)
        console.log(`nilai yang telah diinputkan : ${arrayValue}`);

        if (input === "q" || input === "Q") {
            rl.close();

            data = arrayValue.pop(input);

            // 1. nilai tertinggi
            var highScore = Math.max.apply(Math, arrayValue);
            console.log(`\nnilai tertinggi adalah : ${highScore}\n`);

            // 2. nilai terendah
            var minScore = Math.min.apply(Math, arrayValue);
            console.log(`nilai terendah adalah : ${minScore}\n`);

            // 3. urutkan nilai dari terendah ke tertinggi
            function mergeSort(array) {

                function merge(left, right) {
                    var result = [];
                    lLen = left.length;
                    rLen = right.length;
                    l = 0;
                    r = 0;
                    while (l < lLen && r < rLen) {
                        if (left[l] < right[r]) {
                            result.push(left[l++]);
                        } else {
                            result.push(right[r++]);
                        }
                    }

                    return result.concat(left.slice(l)).concat(right.slice(r));
                }

                function mergSort(arr) {
                    var len = arr.length;
                    if (len < 2) {
                        return arr;
                    }
                    var mid = Math.floor(len / 2);
                    var left = arr.slice(0, mid);
                    var right = arr.slice(mid, len);
                    return merge(mergSort(left), mergSort(right));
                }

                return mergSort(array);
            }
            console.log(`berikut adalah nilai terendah ke tertinggi : \n${mergeSort(arrayValue)}\n`);

            //4. mencari nilai siswa dengan nilai lebih dari 75
            function filterMinValueSiswa(array) {
                var newArray = [];
                for (var i = 0; i < array.length; i++) {
                    if (array[i] < 75) {
                        newArray.push(array[i]);
                    }
                }
                return newArray;
            }

            filterMinValueSiswa(arrayValue);

            function filterValueSiswa(arr) {
                var newArray = [];
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i] >= 75) {
                        newArray.push(arr[i]);
                    }
                }
                return newArray;
            }
            console.log(`dengan aturan nilai 75 ke atas dinyatakan lulus 
            dari nilai ${filterValueSiswa(arrayValue)} dengan berjumlah ${filterValueSiswa(arrayValue).length} siswa
            sementara itu dengan nilai terendah adalah ${filterMinValueSiswa(arrayValue)}
            dengan jumlah ${filterMinValueSiswa(arrayValue).length} siswa`);
        }
    }
}

main();
